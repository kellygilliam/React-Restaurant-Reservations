import React, { Component } from 'react';
import { Redirect } from 'react-router';
import Button from './styleComponents/button'
import './App.css';

class SelectMealAndParty extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
  };
}
  // I chose to keep the redirectIfhaveCorrect methods local for better ease of use and readability
  redirectIfHaveCorrectInput = () => {
    if (this.props.mealValue !== 'Select...' && this.props.numOfPeopleValue !== 'Select...') {
      this.setState({redirect: true});
    }
    else alert('Be sure to make a choice for meal and num of people!');
  }  

  render () {
    if (this.state.redirect === true) {
      return <Redirect to='/selectRestaurant'/>;
    }

    const createDropdown = (item, key) =>
      <option
        key={key}
        value={item.value}
      >
        {item.name}
      </option>;

    return (
      <div className='display-component-wrapper'>
        <div className='options-and-buttons-wrapper'>
          <h4>Select Meal and Number of People</h4>
          <label>
            <span className='custom-dropdown custom-dropdown--white'>
              <select className='custom-dropdown__select custom-dropdown__select--white'
                onChange={
                  event => this.props.updateMealValueAndRemainingOptions(event.target.value)
                }
                value={this.props.mealValue}
              >
                {this.props.mealOptions.map(createDropdown)}
              </select>
              <select className='custom-dropdown__select custom-dropdown__select--white'
                onChange={
                  event => this.props.updateNumOfPeopleValue(event.target.value)
                }
                value={this.props.numOfPeopleValue}
              >
                {this.props.numOfPeopleOptions.map(createDropdown)}
              </select>
            </span>
          </label>
          <div className='btn-wrapper'>
            <Button redirectIfHaveCorrectInput={this.redirectIfHaveCorrectInput} buttonText={'Next'}></Button>
          </div>
        </div>
      </div>
    );
  }
}

export default SelectMealAndParty;