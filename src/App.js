import React, { Component } from 'react';
import './App.css';
import { Switch, Route } from 'react-router-dom';
import { BrowserRouter } from 'react-router-dom'
// import { render } from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
// child components
import SelectMealAndParty from './selectMealAndParty';
import SelectDishAndServings from './selectDishAndServings';
import SelectRestaurant from './selectRestaurant';
import ReviewAndSubmit from './reviewAndSubmit';
// data
import dishesJSON from './data/dishes.json'

/* We'll narrow down to our array of dishes (a property on our JSON data object).. 
..so we can set our intital state to it (at the remainingOptions property below) */
let dishesData = dishesJSON.dishes;

class App extends Component {

  constructor() {
    super();
    this.state = {
      // we will print the reservation object on step 4 submit
      reservationObject: { 
        meal: undefined,
        numOfPeople: 0,
        restaurant: undefined,
        chosenDishes: [{dishName: undefined, noOfServings: 0}]
      },
      // our dishes array of objects from dishes.json
      remainingOptions: dishesData,
      // the resulting state from our filter methods to narrow down our options
      restaurantOptions: [{restaurant: 'Select...'}],
      dishOptions: [{dish: 'Select...'}],

      // ~~ children-specific state to be passed down as props~~:
      // selectMealAndParty
      mealValue: 'Select...',
      numOfPeopleValue: 'Select...',
      mealOptions: [
        {
          name: 'Select...',
          value: null,
        },
        {
          name: 'Breakfast',
          value: 'breakfast',
        },
        {
          name: 'Lunch',
          value: 'lunch',
        },
        {
          name: 'Dinner',
          value: 'dinner',
        },
      ],
      numOfPeopleOptions: [
        {
          name: 'Select...',
          value: null,
        },
        {
          name: '1',
          value: 1,
        },
        {
          name: '2',
          value: 2,
        },
        {
          name: '3',
          value: 3,
        },
        {
          name: '4',
          value: 4,
        },
        {
          name: '5',
          value: 5,
        },
        {
          name: '6',
          value: 6,
        },
        {
          name: '7',
          value: 7,
        },
        {
          name: '8',
          value: 8,
        },
        {
          name: '9',
          value: 9,
        },
        {
          name: '10',
          value: 10
        }
      ],

      // selectRestaurant
      restaurantValue: 'Select...',

      // selectDishAndServings
      chosenDishes: [],
      dropdowns: [],
    };
  }

  // ~~ update reservation object methods ~~
  // in selectMealAndParty 
  updateMealValueAndRemainingOptions = (userChosenMeal) => {
    this.setState({mealValue: userChosenMeal});
    this.updateRemainingOptionsAndChoices('meal', userChosenMeal);
    this.updateMealOnReservationObject(userChosenMeal);
  }
  updateNumOfPeopleValue = (userChosenNumOfPeople) => {
    this.setState({numOfPeopleValue: userChosenNumOfPeople});
    this.updateNumOfPeopleOnReservationObject(userChosenNumOfPeople);
  }
  // in selectRestaurant
  updateRestaurantValueAndRemainingOptions = (userChosenRestaurant) => {
    this.setState({restaurantValue: userChosenRestaurant});
    this.updateRemainingOptionsAndChoices('restaurant', userChosenRestaurant);
    this.updateRestaurantOnReservationObject(userChosenRestaurant);
  }
  // in selectDishAndServingsDropdown
  updateChosenDishes = (thisValue, thisNoOfServings, thisIndex) => {
    /* this method mutates the state directly. It's not recommended to do this.

     I had trouble with the checkForDuplicatesOnChosenDishes method in..
     ..dishAndServingsDropdown and ran out of time to make it work properly..
     ..with a non-mutating approach to this method (such as making a new array.. 
     ..with state.chosenDishes with slice()). 
     If I did: this.state.chosenDishes.slice(), for example,
     ..checkForDiplicatesOnChosenDishes would often not display errors when the dishes in.. 
     ..chosenDishes already had the same value.
     Sorry about this.
    */
    // we are mutating the state here by copying the state:  //
    let newChosenDishes = this.state.chosenDishes;
    // // // // // // // // // // // // // // // // // // // //
    newChosenDishes[thisIndex] = {dishValue: thisValue, noOfServings: thisNoOfServings};
    this.setState({chosenDishes: newChosenDishes});
    // update the reservationObject 
    this.updateChosenDishesOnReservationObject(newChosenDishes);
  }

  // ~~ filters for displaying restaurant and dish options ~~
  // filter restaurants and remove duplicate restaurant names
  filterRestaurants = (newRemainingOptions) => {
    let stringifiedRestaurantsArr = [];
    for (let i = 0; i < newRemainingOptions.length; i++) {
      let restaurant = newRemainingOptions[i].restaurant;
      if(!stringifiedRestaurantsArr.includes(JSON.stringify({restaurant: restaurant}))) stringifiedRestaurantsArr.push(JSON.stringify({restaurant: restaurant}));
    }
    let parsedRestaurantArr = [];
    parsedRestaurantArr[0] = {restaurant: 'Select...'};
    for (let i = 0; i < stringifiedRestaurantsArr.length; i++) {
      parsedRestaurantArr.push(JSON.parse(stringifiedRestaurantsArr[i]));
    }
    // reset chosen dishes
    this.setState({chosenDishes: []});
    return parsedRestaurantArr;
  }
  // filter dishes
  filterDishes = (newRemainingOptions) => {
    let stringifiedDishesArr = [];
    for (let i = 0; i < newRemainingOptions.length; i++) {
      let dish = newRemainingOptions[i].name;
      if(!stringifiedDishesArr.includes(JSON.stringify({name: dish}))) stringifiedDishesArr.push(JSON.stringify({name: dish}));
    }
    let parsedDishesArr = [];
    parsedDishesArr[0] = {name: 'Select...'};
    for (let i = 0; i < stringifiedDishesArr.length; i++) {
      parsedDishesArr.push(JSON.parse(stringifiedDishesArr[i]));
    }
    // reset chosen dishes
    this.setState({chosenDishes: []});
    return parsedDishesArr;
  }

  // ~~ update method that calls the above filters ~~
  updateRemainingOptionsAndChoices = (type, item) => {
    let newRemainingOptions = [];
    let newDishesOptions = [];
    let newRestaurantOptions = [];
    // update restaurant options upon choosing meal
    if (type === 'meal') {
      // find matches in original data
      for (let i = 0; i < dishesData.length; i++) {
        if (dishesData[i].availableMeals.includes(item)) newRemainingOptions.push(dishesData[i]);
      }
      newRestaurantOptions = this.filterRestaurants(newRemainingOptions);
    }
    // update dishes options upon choosing restaurant
    if (type === 'restaurant') {
      // find matches in this state's current remainingOptions
      for (let i = 0; i < this.state.remainingOptions.length; i++) {
        if (this.state.remainingOptions[i].restaurant === item) newRemainingOptions.push(this.state.remainingOptions[i]);
      }
      newDishesOptions = this.filterDishes(newRemainingOptions);
    }
    // if restaurant options is not empty, update remainingOptions
    if (newRestaurantOptions.length !== 0) {
      this.setState({
        remainingOptions: newRemainingOptions, 
        restaurantOptions: newRestaurantOptions,
        restaurantValue: 'Select...'
      });
    }
    else {
      this.setState({dishOptions: newDishesOptions});
    }  
  }

  // ~~ methods to update App's state's reservationObject ~~ 
  updateMealOnReservationObject = (userChosenMeal) => {
    let reservationObject = {...this.state.reservationObject};
    reservationObject.meal = userChosenMeal;
    this.setState({reservationObject});
  }
  updateNumOfPeopleOnReservationObject = (userChosenNumOfPeople) => {
    let reservationObject = {...this.state.reservationObject};
    reservationObject.numOfPeople = userChosenNumOfPeople;
    this.setState({reservationObject});
  }
  updateRestaurantOnReservationObject = (userChosenRestaurant) => {
    let reservationObject = {...this.state.reservationObject};
    reservationObject.restaurant = userChosenRestaurant;
    this.setState({reservationObject});
  }
  updateChosenDishesOnReservationObject = (userChosenDishes) => {
    let reservationObject = {...this.state.reservationObject};
    reservationObject.chosenDishes = userChosenDishes;
    this.setState({reservationObject});
  }

  // ~~ log our final value at our last step ~~
  // reviewAndSubmit
  consoleLogReservationObject = () => {
    console.log(this.state.reservationObject);
  }
  
  render() {
    return (
      <div className="App">
        <h1 id='title'>{'Reservation Portal'}</h1>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" render={props => <SelectMealAndParty
              // state
              mealValue={this.state.mealValue}
              numOfPeopleValue={this.state.numOfPeopleValue}
              mealOptions={this.state.mealOptions}
              numOfPeopleOptions={this.state.numOfPeopleOptions} 
              // methods
              updateMealValueAndRemainingOptions={this.updateMealValueAndRemainingOptions}
              updateNumOfPeopleValue={this.updateNumOfPeopleValue}
              updateMealOnReservationObject={this.updateMealOnReservationObject}
              updateNumOfPeopleOnReservationObject={this.updateNumOfPeopleOnReservationObject}
              {...props} />} 
            />
            <Route path="/selectRestaurant" render={props => <SelectRestaurant 
              // state 
              restaurantValue={this.state.restaurantValue}
              restaurantOptions={this.state.restaurantOptions}
              // methods 
              updateRestaurantValueAndRemainingOptions={this.updateRestaurantValueAndRemainingOptions}
              {...props} />} 
            />
            <Route path="/selectDishAndServings" render={props => <SelectDishAndServings
              // state 
              dishOptions={this.state.dishOptions}
              chosenDishes={this.state.chosenDishes}
              reservationObject={this.state.reservationObject}
              // methods
              updateChosenDishes={this.updateChosenDishes}
              {...props} />} 
            />
            <MuiThemeProvider>
            <Route path="/reviewAndSubmit" render={props => <ReviewAndSubmit
              // state
              reservationObject={this.state.reservationObject}
              // methods
              consoleLogReservationObject={this.consoleLogReservationObject}
              {...props} />} 
            />
            </MuiThemeProvider>
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
