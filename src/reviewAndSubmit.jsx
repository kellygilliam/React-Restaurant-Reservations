import React from 'react';
import { Link } from 'react-router-dom';
import Button from './styleComponents/button';
import Paper from 'material-ui/Paper';
import './App.css';

const ReviewAndSubmit = ( props ) => {

    const style = {
      height: 380,
      width: 500,
      margin: 20,
      textAlign: 'center',
      display: 'inline-block',
    };
  
    let chosenDishesList = [];
    for (let i = 0; i < props.reservationObject.chosenDishes.length; i++) {
      if (props.reservationObject.chosenDishes[i].noOfServings !== '0' && props.reservationObject.chosenDishes[i].dishValue !== 'Select...')  {
        chosenDishesList.push(<div key={i}>{props.reservationObject.chosenDishes[i].dishValue + ' - ' + props.reservationObject.chosenDishes[i].noOfServings}</div>)
      }
    }
    return (
      <div className='display-component-wrapper'>
        <div className='options-and-buttons-wrapper'>
          <Paper style={style} zDepth={3}>
            <h4>Review and Submit</h4>
            <div id='reservation-details-wrapper'>
              <div className='reservation-detail-left'>{'Meal:'}</div><div className='reservation-detail-right'>{props.reservationObject.meal}</div>
              <br></br>
              <br></br>
              <div className='reservation-detail-left'>{'No. of People:'}</div><div className='reservation-detail-right'>{props.reservationObject.numOfPeople}</div>
              <br></br>
              <br></br>
              <div className='reservation-detail-left'>{'Restaurant:'}</div><div className='reservation-detail-right'>{props.reservationObject.restaurant}</div>
              <br></br>
              <br></br>
              <div className= 'reservation-detail-left'>{'Dishes:'}</div><div className='reservation-detail-right'>{chosenDishesList}</div>
            </div>
          </Paper>
          <div className='btn-wrapper'>
            <Link to='/selectDishAndServings' style={{ textDecoration: 'none' }}>
              <Button buttonText={'Previous'}></Button>
            </Link>
            <Button consoleLogReservationObject={props.consoleLogReservationObject} buttonText={'Submit'}></Button>
          </div>
        </div>
      </div>
    );
}

export default ReviewAndSubmit;