import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';
import Button from './styleComponents/button';
import AddButton from './styleComponents/addButton';
import DishAndServingsDropdown from './subComponents/dishAndServingsDropdown';
import './App.css';

class SelectDishAndServings extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
      maxDishesAllowed: 10,
      maxButtons: this.props.dishOptions.length-1,
      chosenDishes: [],
      dropdowns: [],
    };
  }
    /* NOTE: setting state in componentWillMount can be dangerous because
     React's setState() is asynchronous. Just to be safe, though it will cause a 
     rerender, we should practice setting state in ComponentDIDMount instead */
    componentDidMount = () => {
      /* do if we haven't chosen any dishes yet:
       (This will happen on the very first visit to this 'page' AND when the user goes back to an earlier step..  
      ..and chooses another meal or restaurant) */
      // gives us one brand new dropdown
      if (this.props.chosenDishes.length === 0) {
        this.addDropdown();
      }
      /* otherwise, if we have been to this 'page' before, add Dropdowns based on our previously selected.. 
      ..chosenDishes in our top level state so our previous selections appear */
      // gives us our previous dropdowns
      else if (this.props.chosenDishes.length > 0) {
        this.addDropdownsFromChosenDishes();
      }
    }

    // I chose to keep the redirectIfhaveCorrect methods local for better ease of use and readability
    redirectIfHaveCorrectInput = () => {
      /* loop through chosenDishes:
       if there are not enough dishes to feed the number of people we have, send an alert
       otherwise, set redirect to true */
      let sumNoOfServings = 0;
      for (let i = 0; i < this.props.chosenDishes.length; i++) {
        // don't add the number of dishes for those choices with no valid option selected
        if (this.props.chosenDishes[i].dishValue !== 'Select...') {
          sumNoOfServings += Number(this.props.chosenDishes[i].noOfServings);
        }
      }
      // alert if user chooses more than 10 dishes total
      if (sumNoOfServings > 10) {
        alert('Please keep your orders to 10 or less dishes');
      }
      // if we have enough, set redirect to true
      else if (sumNoOfServings >= this.props.reservationObject.numOfPeople) {
        this.setState({redirect: true});
      }
      // otherwise, alert
      else alert ('Be sure to get enough food for everybody!');
    }

    addDropdown = () => {
      let newDropDowns = [];

      for (let i = 0; i < this.state.dropdowns.length; i++) {
        newDropDowns.push(this.state.dropdowns[i]);
      }
      newDropDowns.push(
        <DishAndServingsDropdown
          key={newDropDowns.length+1}
          initialDishValue={'Select...'}
          initialNoOfServings={'1'}
          index={this.state.dropdowns.length}
          dishOptions={this.props.dishOptions}
          updateChosenDishes={this.props.updateChosenDishes}
          chosenDishes={this.props.chosenDishes}
        >
        </DishAndServingsDropdown>
      )
      this.setState({dropdowns: newDropDowns});
    }

    addDropdownsFromChosenDishes = () => {
      let dropDownsFromChosenDishes = [];
      for (let i = 0; i < this.props.chosenDishes.length; i++) {
        let initialDishValue = this.props.chosenDishes[i].dishValue;
        let initialNoOfServings = this.props.chosenDishes[i].noOfServings;
        dropDownsFromChosenDishes.push(
          <DishAndServingsDropdown
            key={i}
            initialDishValue={initialDishValue}
            initialNoOfServings={initialNoOfServings}
            index={i}
            dishOptions={this.props.dishOptions}
            updateChosenDishes={this.props.updateChosenDishes}
            chosenDishes={this.props.chosenDishes}
          >
          </DishAndServingsDropdown>
        )
      }
      this.setState({dropdowns: dropDownsFromChosenDishes});
    }
  
    callAddDropdownIfWithinNumOfDishes = () => {
      if (this.state.dropdowns.length < this.state.maxButtons) this.addDropdown();
    }
  
    render () {
      
      if (this.state.redirect === true) {
        return <Redirect to='/reviewAndSubmit'/>; 
      }
      
      return (
        <div className='display-component-wrapper'>
          <div className='options-and-buttons-wrapper'>
            <h4>Choose dishes and amounts</h4>
              <div>
                {this.state.dropdowns}
                </div>
                <div className='btn-wrapper'>
                  <AddButton 
                    callAddDropdownIfWithinNumOfDishes={this.callAddDropdownIfWithinNumOfDishes}
                    >
                  </AddButton>
                  <Link to='/selectRestaurant' style={{ textDecoration: 'none' }}>
                    <Button buttonText={'Previous '}></Button>
                  </Link>
                  <Button redirectIfHaveCorrectInput={this.redirectIfHaveCorrectInput} buttonText={'Next'}></Button>
              </div>
          </div>
        </div>
      );
    }
}

export default SelectDishAndServings;