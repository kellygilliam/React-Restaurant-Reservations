import * as testFunctions from './../testFunctions/functionsToTest'

// test filterChosenDishes (2)
const dishesInput = [{dishValue: "Fruit Salad", noOfServings: "3"}, {dishValue: "Select...", noOfServings: "3"}];
const expectedDishChoicesOutput = [{dishValue: "Fruit Salad", noOfServings: "3"}];

test('doesnt return any properties with dishNames of "Select..."', () => {
  expect(testFunctions.filterChosenDishes(dishesInput)).toEqual(expectedDishChoicesOutput);
});

const dishesInput2 = [{dishValue: "Fruit Salad", noOfServings: "3"}, {dishValue: "Vege Salad", noOfServings: "0"}];
const expectedDishChoicesOutput2 = [{dishValue: "Fruit Salad", noOfServings: "3"}];

test('doesnt return any properties with noOf of "Select..."', () => {
  expect(testFunctions.filterChosenDishes(dishesInput2)).toEqual(expectedDishChoicesOutput2);
});


// test filterRestaurants 
const remainingOptionsInput = [
  {id: 5, name: "Egg Muffin", restaurant: "Mc Donalds", availableMeals: Array(1)},
  {id: 14, name: "Coleslaw Sandwich", restaurant: "Vege Deli", availableMeals: Array(1)},
  {id: 15, name: "Grilled Sandwich", restaurant: "Vege Deli", availableMeals: Array(1)},
  {id: 30, name: "Garlic Bread", restaurant: "Olive Garden", availableMeals: Array(3)}
]
const expectedMealOutput = [
  {restaurant: "Select..."},
  {restaurant: "Mc Donalds"},
  {restaurant: "Vege Deli"},
  {restaurant: "Olive Garden"}
];

test('filters out restaurants and returns no duplicate values', () => {
  expect(testFunctions.filterRestaurants(remainingOptionsInput)).toEqual(expectedMealOutput);
});


// test filterDishes 
const remainingOptionsInput2 = [
  {id: 14, name: "Coleslaw Sandwich", restaurant: "Vege Deli", availableMeals: Array(1)},
  {id: 15, name: "Grilled Sandwich", restaurant: "Vege Deli", availableMeals: Array(1)}
]
const expectedDishesOutput = [
  {name: "Select..."},
  {name: "Coleslaw Sandwich"},
  {name: "Grilled Sandwich"}
] 

test('filters out dishes', () => {
  expect(testFunctions.filterDishes(remainingOptionsInput2)).toEqual(expectedDishesOutput);
});

// test updateRemainingOptions when choosing meal
const expectedRemainingOptionsOutput = [
  {id: 5, name: "Egg Muffin", restaurant: "Mc Donalds", availableMeals: ["breakfast"]},
  {id: 14, name: "Coleslaw Sandwich", restaurant: "Vege Deli", availableMeals: ["breakfast"]},
  {id: 15, name: "Grilled Sandwich", restaurant: "Vege Deli", availableMeals: ["breakfast"]},
  {id: 30, name: "Garlic Bread", restaurant: "Olive Garden", availableMeals: ["breakfast", "lunch", "dinner"]}
]

test('returns array of objects that contain "breakfast" in thier "availableMeals" property', () => {
  expect(testFunctions.updateRemainingOptionsAndChoices('meal', 'breakfast')).toEqual(expectedRemainingOptionsOutput);
})

