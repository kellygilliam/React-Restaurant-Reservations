import React from 'react';
import './../App.css';

const addButtonStyle =  {
  background:'#ff6666',
  width:'40px',
  height:'40px',
  border:'1px',
  borderRadius: '50%',
  color:'white',
  borderBottom:'4px solid #FF5252',
  fontSize:'20px',
  textShadow:'1px #FF5252',
  textTransform:'uppercase'
}

const AddButton = (props) => {
  return (
    <button className='.Add-button' style={addButtonStyle} onClick={ () => {props.callAddDropdownIfWithinNumOfDishes()}}>{'+'}</button>
  )
}

export default AddButton;