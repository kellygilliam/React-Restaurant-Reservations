import React from 'react';
import './../App.css';

const reactButtonStyle =  {
  background:'#ff6666',
  width:'110px',
  height:'40px',
  border:'0px',
  color:'white',
  borderBottom:'4px solid #FF5252',
  borderRadius:'2px',
  fontSize:'20px',
  textShadow:'1px #FF5252',
  textTransform:'uppercase',
  marginRight:'2px',
  marginLeft:'2px'
}

const ReactButton = (props) => {
  // for the first two conditions, only handle clicks in button if the below props are passed in
  if (props.redirectIfHaveCorrectInput) {
    return (
      <button className='.React-button' style={reactButtonStyle} onClick={()=> {props.redirectIfHaveCorrectInput()}}>{props.buttonText}</button>
    )
  }
  if (props.consoleLogReservationObject) {
    return (
      <button className='.React-button' style={reactButtonStyle} onClick={()=> {props.consoleLogReservationObject()}}>{props.buttonText}</button>
    )
  }
  /* if neither of the above props are passed, render our standard button.. 
  ..which will provide a 'button' style to our React Router <Link>s */
  return <button className='.React-button' style={reactButtonStyle}>{props.buttonText}</button>
}

export default ReactButton;