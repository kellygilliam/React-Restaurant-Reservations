import dishesJSON from './../data/dishes.json'
const dishesData = dishesJSON.dishes; 

export function filterChosenDishes (chosenDishes) {
  let chosenDishesList = [];
  for (let i = 0; i < chosenDishes.length; i++) {
    if (chosenDishes[i].noOfServings !== '0' && chosenDishes[i].dishValue !== 'Select...')  {
      chosenDishesList.push({dishValue: chosenDishes[i].dishValue, noOfServings: chosenDishes[i].noOfServings})
    }
  }
  return chosenDishesList;
}

export function filterRestaurants (newRemainingOptions) {
  let stringifiedRestaurantsArr = [];
  for (let i = 0; i < newRemainingOptions.length; i++) {
    let restaurant = newRemainingOptions[i].restaurant;
    if(!stringifiedRestaurantsArr.includes(JSON.stringify({restaurant: restaurant}))) stringifiedRestaurantsArr.push(JSON.stringify({restaurant: restaurant}));
  }
  let parsedRestaurantArr = [];
  parsedRestaurantArr[0] = {restaurant: 'Select...'};
  for (let i = 0; i < stringifiedRestaurantsArr.length; i++) {
    parsedRestaurantArr.push(JSON.parse(stringifiedRestaurantsArr[i]));
  }
  return parsedRestaurantArr;
}

export function filterDishes (newRemainingOptions) {
  let stringifiedDishesArr = [];
  for (let i = 0; i < newRemainingOptions.length; i++) {
    let dish = newRemainingOptions[i].name;
    if(!stringifiedDishesArr.includes(JSON.stringify({name: dish}))) stringifiedDishesArr.push(JSON.stringify({name: dish}));
  }
  let parsedDishesArr = [];
  parsedDishesArr[0] = {name: 'Select...'};
  for (let i = 0; i < stringifiedDishesArr.length; i++) {
    parsedDishesArr.push(JSON.parse(stringifiedDishesArr[i]));
  }
  return parsedDishesArr;
}

// example remaining options based on an earlier meal selection of 'breakfast'
const remainingOptions = [
  {id: 5, name: "Egg Muffin", restaurant: "Mc Donalds", availableMeals: ["breakfast"]},
  {id: 14, name: "Coleslaw Sandwich", restaurant: "Vege Deli", availableMeals: ["breakfast"]},
  {id: 15, name: "Grilled Sandwich", restaurant: "Vege Deli", availableMeals: ["breakfast"]},
  {id: 30, name: "Garlic Bread", restaurant: "Olive Garden", availableMeals: ["breakfast", "lunch", "dinner"]}
];

export function updateRemainingOptionsAndChoices (type, item) {
  let newRemainingOptions = [];
  let newDishesOptions = [];
  let newRestaurantOptions = [];
  // update restaurant options upon choosing meal
  if (type === 'meal') {
    // find matches in original data
    for (let i = 0; i < dishesData.length; i++) {
      if (dishesData[i].availableMeals.includes(item)) newRemainingOptions.push(dishesData[i]);
    }
    return newRemainingOptions;
  }
  // update dishes options upon choosing restaurant
  if (type === 'restaurant') {
    // find matches in this state's current remainingOptions
    for (let i = 0; i < remainingOptions.length; i++) {
      if (remainingOptions[i].restaurant === item) newRemainingOptions.push(remainingOptions[i]);
    }
    return newRemainingOptions;
  } 
}
