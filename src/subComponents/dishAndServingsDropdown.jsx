import React, { Component } from 'react';
import './../App.css';

class SelectRestaurant extends Component {

  constructor(props) {
    super(props);
      this.state = {
        dishValue: this.props.initialDishValue,
        noOfServingsValue: this.props.initialNoOfServings,
        index: this.props.index,
        noOfServingsOptions: [
          {
            name: '0',
            value: '0'
          },
          {
            name: '1',
            value: '1',
          },
          {
            name: '2',
            value: '2'
          },
          {
            name: '3',
            value: '3'
          },
          {
            name: '4',
            value: '4',
          },
          {
            name: '5',
            value: '5'
          },
          {
            name: '6',
            value: '6'
          },
          {
            name: '7',
            value: '7',
          },
          {
            name: '8',
            value: '8'
          },
          {
            name: '9',
            value: '9',
          },
          {
            name: '10',
            value: '10'
          }
        ]
      };
  }
  
  updateDishValue = (userChosenDishValue) => {
    this.setState({dishValue: userChosenDishValue})
  }

  updateNoOfServings = (userChosenNoOfServings) => {
      this.setState({noOfServingsValue: userChosenNoOfServings});
      this.props.updateChosenDishes(this.state.dishValue, userChosenNoOfServings, this.state.index);
   }

  checkForDuplicatesOnChosenDishes = (userChosenDishValue) => {
    // loop through chosen dishes
    for (let i = 0; i < this.props.chosenDishes.length; i++) {
      if (i !== this.index) {
        // if a dishValue property that is not on the element of this index has this dishValue, alert the user
        // filter out 'Select...' values so we don't display them
       if(this.props.chosenDishes[i].dishValue !== 'Select...') {
          if (this.props.chosenDishes[i].dishValue === userChosenDishValue){
           alert('dish already chosen!');
            return;
          } 
        }
      }
    }
    // otherwise, update this state and add the userChosenDishValue and this noOfServings value
    this.updateDishValue(userChosenDishValue);
    this.props.updateChosenDishes(userChosenDishValue, this.state.noOfServingsValue, this.state.index);
  }


  render() {

    const createDropdown = (item, key) =>
      <option
        key={key}
        value={item.name}
      >
        {item.name}
      </option>;

    return (
      <div id='dish-and-servings-dropdown-wrapper'>
        <label>
          <span className='custom-dropdown custom-dropdown--white'>
            <select className='custom-dropdown__select custom-dropdown__select--white'
              onChange={
                event => this.checkForDuplicatesOnChosenDishes(event.target.value)
              }
              value={this.state.dishValue}
            >
              {this.props.dishOptions.map(createDropdown)}
            </select>
            <select className='custom-dropdown__select custom-dropdown__select--white'
              onChange={
                event => this.updateNoOfServings(event.target.value)
              }
              value={this.state.noOfServingsValue}
            >
              {this.state.noOfServingsOptions.map(createDropdown)}
            </select>
          </span>
        </label>
      </div>
    );
  }
}

export default SelectRestaurant;