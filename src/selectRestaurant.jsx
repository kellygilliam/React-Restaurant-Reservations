import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Redirect } from 'react-router';
import Button from './styleComponents/button';
import './App.css';

class SelectRestaurant extends Component {

  constructor(props) {
    super(props);
    this.state = {
      redirect: false,
    }
  }

  // I chose to keep the redirectIfhaveCorrect methods local for better ease of state management and for readability
  redirectIfHaveCorrectInput = () => {
    if (this.props.restaurantValue !== 'Select...') {
      this.setState({redirect: true});
    }
    else alert('Be sure to choose a restaurant!');
  }

  render() {

    if (this.state.redirect === true) {
      return <Redirect to='/selectDishAndServings'/>;
    }

    const createDropdown = (item, key) =>
      <option
        key={key}
        value={item.restaurant}
      >
        {item.restaurant}
      </option>;

    return (
      <div className='display-component-wrapper'>
        <div className='options-and-buttons-wrapper'>
          <h4>Choose a Restaurant</h4>
          <label>
            <span className='custom-dropdown custom-dropdown--white'>
              <select className='custom-dropdown__select custom-dropdown__select--white'
                onChange={
                  event => this.props.updateRestaurantValueAndRemainingOptions(event.target.value)
                }
                value={this.props.restaurantValue}
              >
                {this.props.restaurantOptions.map(createDropdown)}
              </select>
            </span>
          </label>
          <div className='btn-wrapper'>
            <Link to='/' style={{ textDecoration: 'none' }}>
              <Button buttonText={'Previous '}></Button>
            </Link>
            <Button redirectIfHaveCorrectInput={this.redirectIfHaveCorrectInput} buttonText={'Next'}></Button>
          </div>
        </div>
      </div>
    );
  }
}

export default SelectRestaurant;